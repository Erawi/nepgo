package layout;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.erawi.nepgo.Activity.CreatePostActivity;
import com.example.erawi.nepgo.Activity.LoginActivity;
import com.example.erawi.nepgo.Activity.RegisterActivity;
import com.example.erawi.nepgo.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SubPage01.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SubPage01#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubPage01 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final String BASE_URL = "http://nepgo.herokuapp.com/";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Button newPost;

    private OnFragmentInteractionListener mListener;

    public SubPage01() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubPage01.
     */
    // TODO: Rename and change types and number of parameters
    public static SubPage01 newInstance(String param1, String param2) {
        SubPage01 fragment = new SubPage01();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sub_page01, container, false);

        /*String[] testStrings = {"Test Tester", "TestSector", "1.2.2017", "TestPost", "TestDesc", "Test Commenter", "TestComment"};
        ListView listView = (ListView)view.findViewById(R.id.timelineListView);
        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, testStrings);
        listView.setAdapter(adapter); */

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

/*    private void loadPosts(){
        ApiInterface mApiInterface = RestClient.getClient();

        Call<List<Timeline>> loadPostsCall = mApiInterface.loadTimeline();
        loadPostsCall.enqueue(new Callback<List<Timeline>>() {
            @Override
            public void onResponse(Call<List<Timeline>> call, Response<List<Timeline>> response) {
                List<Timeline> timeline = response.body();
                for(int i = 1; i < timeline.size(); i++){
                    TextView tvName = (TextView)getView().findViewById(R.id.tvName);
                    TextView tvSector = (TextView)getView().findViewById(R.id.tvSector);
                    TextView tvDate = (TextView)getView().findViewById(R.id.tvDate);
                    TextView tvTitle = (TextView)getView().findViewById(R.id.tvTitle);
                    TextView tvDescription = (TextView)getView().findViewById(R.id.tvDescription);

                    String tName = timeline.get(i).getVacancy().getUser().getName();
                    String tSector = timeline.get(i).getVacancy().getSector();
                    String tDate = timeline.get(i).getVacancy().getCreated();
                    String tTitle = timeline.get(i).getVacancy().getTitle();
                    String tDesc = timeline.get(i).getVacancy().getDescription();

                    if(timeline.get(i).getVacancy() != null){
                        tvName.setText(tName);
                        tvSector.setText(tSector);
                        tvDate.setText(tDate);
                        tvTitle.setText(tTitle);
                        tvDescription.setText(tDesc);
                    }
                }
                Log.d("TimelineActivity: ", response.body().toString());
            }

            @Override
            public void onFailure(Call<List<Timeline>> call, Throwable t) {
                Log.d("TimelineActivity: ", t.getMessage());
            }
        });
    }*/

}