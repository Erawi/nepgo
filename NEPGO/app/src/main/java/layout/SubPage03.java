package layout;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.example.erawi.nepgo.Adapter.ExListAdapter;
import com.example.erawi.nepgo.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class SubPage03 extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ExpandableListView expandableListView;
    View v;
    Context con;
    private List<String> list;
    private Map<String, List<String>> info;
    private ExpandableListAdapter listAdapter;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public SubPage03() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SubPage03.
     */
    // TODO: Rename and change types and number of parameters
    public static SubPage03 newInstance(String param1, String param2) {
        SubPage03 fragment = new SubPage03();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_sub_page03, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        con = getActivity();
        //expandableListView = (ExpandableListView)v.findViewById(R.id.expList);
        expandableListView = (ExpandableListView)v.findViewById(R.id.expandableListView);
        fillData();

        listAdapter = new ExListAdapter(con, list, info);

        expandableListView.setGroupIndicator(null);
        expandableListView.setAdapter(listAdapter);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void fillData(){
        list = new ArrayList<>();
        info = new HashMap<>();

        list.add((getResources().getString(R.string.who)));
        list.add((getResources().getString(R.string.how)));
        list.add((getResources().getString(R.string.edu)));
        list.add((getResources().getString(R.string.business)));
        list.add((getResources().getString(R.string.expert)));
        list.add((getResources().getString(R.string.organization)));
        list.add((getResources().getString(R.string.student)));
        list.add((getResources().getString(R.string.visitor)));

        List<String> a1  = new ArrayList<>();
        List<String> a2 = new ArrayList<>();
        List<String> a3 = new ArrayList<>();
        List<String> a4 = new ArrayList<>();
        List<String> a5 = new ArrayList<>();
        List<String> a6 = new ArrayList<>();
        List<String> a7 = new ArrayList<>();
        List<String> a8 = new ArrayList<>();

        a1.add((getResources().getString(R.string.add1)));
        a2.add((getResources().getString(R.string.add2)));
        a3.add((getResources().getString(R.string.add3)));
        a4.add((getResources().getString(R.string.add4)));
        a5.add((getResources().getString(R.string.add5)));
        a6.add((getResources().getString(R.string.add6)));
        a7.add((getResources().getString(R.string.add7)));
        a8.add((getResources().getString(R.string.add8)));

        info.put(list.get(0),a1);
        info.put(list.get(1),a2);
        info.put(list.get(2),a3);
        info.put(list.get(3),a4);
        info.put(list.get(4),a5);
        info.put(list.get(5),a6);
        info.put(list.get(6),a7);
        info.put(list.get(7),a8);
    }

}
