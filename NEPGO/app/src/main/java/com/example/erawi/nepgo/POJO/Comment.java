package com.example.erawi.nepgo.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

    @SerializedName("user")
    @Expose
    private TimelineUser user;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("approved")
    @Expose
    private Boolean approved;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public TimelineUser getUser() {
        return user;
    }

    public void setUser(TimelineUser user) {
        this.user = user;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


}