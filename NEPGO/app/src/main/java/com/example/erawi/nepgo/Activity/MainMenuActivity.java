package com.example.erawi.nepgo.Activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.example.erawi.nepgo.Adapter.ExListAdapter;
import com.example.erawi.nepgo.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.res.Configuration;
import java.util.Locale;

public class MainMenuActivity extends FragmentActivity {

    private ExpandableListView expandableListView;
    private List<String> list;
    private Map<String, List<String>> info;
    private ExpandableListAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        final SharedPreferences.Editor lng_edit = lng_pref.edit();
        final SharedPreferences lng_count_pref = getSharedPreferences("lng_count_sp", MODE_PRIVATE);
        final SharedPreferences.Editor lng_count_edit = lng_count_pref.edit();

        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        this.setContentView(R.layout.activity_main_menu);

        setContentView(R.layout.activity_main_menu);

        expandableListView = (ExpandableListView)findViewById(R.id.expandableListView);
        expandableListView.setGroupIndicator(null);
        fillData();
        //getSupportActionBar().setDisplayShowTitleEnabled(false);

        Button languageButton = (Button)findViewById(R.id.language_button);
        languageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int lng_count = lng_count_pref.getInt("lng_count", 0);

                if(lng_count == 0) {
                    lng_edit.putString("language", "ne");
                    lng_edit.commit();
                    lng_count_edit.putInt("lng_count", 1);
                    lng_count_edit.commit();

                    NotificationCompat.Builder login_notification = new NotificationCompat.Builder(getApplicationContext());
                    login_notification.setSmallIcon(R.drawable.notification_icon1);
                    login_notification.setContentTitle((getResources().getString(R.string.language_change_n)));
                    login_notification.setContentText((getResources().getString(R.string.language_change)));

                    final int notificationID = 111;
                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    // notificationID allows you to update the notification later on.
                    mNotificationManager.notify(notificationID, login_notification.build());
                } else {
                    lng_edit.putString("language", "en");
                    lng_edit.commit();
                    lng_count_edit.putInt("lng_count", 0);
                    lng_count_edit.commit();

                    NotificationCompat.Builder login_notification = new NotificationCompat.Builder(getApplicationContext());
                    login_notification.setSmallIcon(R.drawable.notification_icon1);
                    login_notification.setContentTitle((getResources().getString(R.string.language_change_n)));
                    login_notification.setContentText((getResources().getString(R.string.language_change)));

                    final int notificationID = 111;
                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    // notificationID allows you to update the notification later on.
                    mNotificationManager.notify(notificationID, login_notification.build());
                }
                //finish();
                //startActivity(getIntent());
                recreate();
            }
        });
    }

    // Fill the expandable listview
    public void fillData(){
        list = new ArrayList<>();
        info = new HashMap<>();
        list.add((getResources().getString(R.string.who)));
        list.add((getResources().getString(R.string.how)));
        list.add((getResources().getString(R.string.edu)));
        list.add((getResources().getString(R.string.business)));
        list.add((getResources().getString(R.string.expert)));
        list.add((getResources().getString(R.string.organization)));
        list.add((getResources().getString(R.string.student)));
        list.add((getResources().getString(R.string.visitor)));
        List<String> a1  = new ArrayList<>();
        List<String> a2 = new ArrayList<>();
        List<String> a3 = new ArrayList<>();
        List<String> a4 = new ArrayList<>();
        List<String> a5 = new ArrayList<>();
        List<String> a6 = new ArrayList<>();
        List<String> a7 = new ArrayList<>();
        List<String> a8 = new ArrayList<>();
        a1.add((getResources().getString(R.string.add1)));
        a2.add((getResources().getString(R.string.add2)));
        a3.add((getResources().getString(R.string.add3)));
        a4.add((getResources().getString(R.string.add4)));
        a5.add((getResources().getString(R.string.add5)));
        a6.add((getResources().getString(R.string.add6)));
        a7.add((getResources().getString(R.string.add7)));
        a8.add((getResources().getString(R.string.add8)));
        info.put(list.get(0),a1);
        info.put(list.get(1),a2);
        info.put(list.get(2),a3);
        info.put(list.get(3),a4);
        info.put(list.get(4),a5);
        info.put(list.get(5),a6);
        info.put(list.get(6),a7);
        info.put(list.get(7),a8);

        listAdapter = new ExListAdapter(this, list, info);
        expandableListView.setAdapter(listAdapter);
    }

    public void launchLoginActivity(View v){
        startActivity(new Intent(MainMenuActivity.this, LoginActivity.class));
    }

}
