package com.example.erawi.nepgo.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.example.erawi.nepgo.R;

/**
 * Created by Erawi on 31.5.2017.
 */

public class TimelineAdapter extends RecyclerView.ViewHolder{

    public TextView name, sector, date, title, desc, comment, commentName, likes, commentCount;
    public EditText commentLine;
    public ImageButton likeButton;
    public ImageView image;
    public Button sendButton;
    public VideoView videoView;

    public TimelineAdapter(View view){
        super(view);

        name = (TextView)view.findViewById(R.id.tvName);
        sector = (TextView)view.findViewById(R.id.tvSector);
        date = (TextView)view.findViewById(R.id.tvDate);
        title = (TextView)view.findViewById(R.id.tvTitle);
        desc = (TextView)view.findViewById(R.id.tvDescription);
        comment = (TextView)view.findViewById(R.id.tvComment);
        commentName = (TextView)view.findViewById(R.id.tvCommentName);
        likes = (TextView)view.findViewById(R.id.tvLikes);
        commentCount = (TextView)view.findViewById(R.id.tvCommentsCount);
        likeButton = (ImageButton)view.findViewById(R.id.btnLike);
        sendButton = (Button)view.findViewById(R.id.sendCommentBtn);
        commentLine = (EditText)view.findViewById(R.id.commentLine);
        image = (ImageView)view.findViewById(R.id.postedImage);
        videoView = (VideoView)view.findViewById(R.id.videoView);
    }
}
