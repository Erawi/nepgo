package com.example.erawi.nepgo.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Like {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("educations")
    @Expose
    private List<Object> educations = null;
    @SerializedName("skills")
    @Expose
    private List<Object> skills = null;
    @SerializedName("sectors")
    @Expose
    private List<Object> sectors = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getEducations() {
        return educations;
    }

    public void setEducations(List<Object> educations) {
        this.educations = educations;
    }

    public List<Object> getSkills() {
        return skills;
    }

    public void setSkills(List<Object> skills) {
        this.skills = skills;
    }

    public List<Object> getSectors() {
        return sectors;
    }

    public void setSectors(List<Object> sectors) {
        this.sectors = sectors;
    }

}