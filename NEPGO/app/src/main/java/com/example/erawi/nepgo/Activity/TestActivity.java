package com.example.erawi.nepgo.Activity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.S3Sign;
import com.example.erawi.nepgo.R;
import com.ipaulpro.afilechooser.utils.FileUtils;
import com.kosalgeek.android.photoutil.GalleryPhoto;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Url;

public class TestActivity extends AppCompatActivity {

    private String uploadUri, previewUri, baseURL, finalUploadUri, decodedUri, token; //fileType;
    private GalleryPhoto galleryPhoto;
    private File originalFile;
    private int PICK_IMAGE_FROM_GALLERY_REQUEST = 1;
    private int PICK_FILE_FROM_DEVICE = 1;
    private Uri uri;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_test);
        galleryPhoto = new GalleryPhoto(getApplicationContext());

        // Get user token from sharedpref
        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        token = sharedPref.getString("token", "");
        //fileType = "image/png";
        baseURL = "https://test-prata.s3.amazonaws.com/";
        img = (ImageView)findViewById(R.id.img);

        Button uploadBtn = (Button)findViewById(R.id.uploadbtn);
        uploadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO: HANDLE SAMSUNG DEVICES
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, PICK_FILE_FROM_DEVICE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_FILE_FROM_DEVICE && resultCode == RESULT_OK && data != null && data.getData() != null){
            uri = data.getData();
            Log.d("uri: ", uri.toString());
            ContentResolver cr = getContentResolver();
            String mimetype = cr.getType(uri);
            Log.d("mimetype: ", mimetype);
            getSignedUri(mimetype);
        }
    }

    public void getSignedUri(final String fileType){
        ApiInterface service = RestClient.getClient();
        Call<S3Sign> call = service.getSignedUrl(token, fileType);
        call.enqueue(new Callback<S3Sign>() {
            @Override
            public void onResponse(Call<S3Sign> call, Response<S3Sign> response) {
                int statusCode = response.code();
                S3Sign s3Sign = response.body();
                uploadUri = s3Sign.getSignedRequest();
                previewUri = s3Sign.getUrl();

                if(statusCode == 200){
                    Log.d("SignedRequest: ", uploadUri);
                    Log.d("URL: ", previewUri);
                    finalUploadUri = uploadUri.replace("https://test-prata.s3.amazonaws.com/", "");
                    try{
                        decodedUri = URLDecoder.decode(finalUploadUri, "UTF-8");
                    }catch (UnsupportedEncodingException e){
                        e.printStackTrace();
                    }
                    uploadFile(uri, fileType);
                }
                else {
                    Toast.makeText(TestActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<S3Sign> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });
    }

    public void uploadFile(Uri fileUri, String fileType) {
        originalFile = FileUtils.getFile(this, fileUri);
        String pathToFile = originalFile.getPath();
        File fileToBody = new File(pathToFile);

        RequestBody file = RequestBody.create(MediaType.parse(fileType), fileToBody);

        // Add logging into retrofit
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.interceptors().add(logging);

        //Create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build());
        Retrofit retrofit = builder.build();
        ApiInterface client = retrofit.create(ApiInterface.class);

        Log.d("uploadURI: ", finalUploadUri);

        Call<ResponseBody> call = client.uploadImage(finalUploadUri, file);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if(statusCode == 200){
                    Toast.makeText(TestActivity.this, "Success!", Toast.LENGTH_SHORT).show();
                    Glide.with(getApplicationContext()).load(previewUri).into(img);
                    //TODO: CREATE POST HERE
                }
                else{
                    Toast.makeText(TestActivity.this, "Failure " + statusCode, Toast.LENGTH_SHORT).show();
                    Log.d("Failure: ", response.message());
                    Log.d("Failure: ", response.headers().toString());
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TestActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                Log.d("Failure: ", t.getMessage());
                t.printStackTrace();
            }
        });
    }
}
