package com.example.erawi.nepgo.Interface;

import com.example.erawi.nepgo.POJO.Comment;
import com.example.erawi.nepgo.POJO.LoginRequest;
import com.example.erawi.nepgo.POJO.LoginResponse;
import com.example.erawi.nepgo.POJO.Post;
import com.example.erawi.nepgo.POJO.Register;
import com.example.erawi.nepgo.POJO.S3Sign;
import com.example.erawi.nepgo.POJO.Timeline;
import com.example.erawi.nepgo.POJO.User;

import java.io.File;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Erawi on 22.3.2017.
 */

public interface ApiInterface {
    // Login user
    @POST("/login/")
    Call<LoginResponse> loginUser(@Body LoginRequest loginRequest);

    // Register user
    @POST("/user")
    Call<Register> registerUser(@Body Register register);

    // Get s3 signed url
    @GET("/sign-s3")
    Call<S3Sign> getSignedUrl(
            @Query("token") String token,
            @Query("file-type") String fileType);

    // Upload image to s3
    @PUT
    Call<ResponseBody> uploadImage(
            @Url String finalUploadUri,
            @Body RequestBody file
    );

    // Edit user info
    @PUT("/user/{id}")
    Call<User> updateUser(
            @Path("id") String id,
            @Query("token") String token,
            @Body User user);

    // Load newsfeed
    @GET("/post")
    Call<List<Timeline>> loadTimeline();

    // Load user info
    @GET("/user")
    Call<List<User>> loadUser(
            @Query("id") String id);

    // Create a new post
    @POST("/post")
    Call<Post> createNewPost(
            @Query("token") String token,
            @Body Post post);

    // Like a post
    @PUT("/post/{post_id}/like")
    Call<Post> likePost(
            @Path("post_id") String post_id,
            @Query("token") String token);

    // Comment on a post
    @POST("/post/{post_id}/comment")
    Call<Comment> commentPost(
            @Path("post_id") String post_id,
            @Query("token") String token,
            @Body Comment comment);
}
