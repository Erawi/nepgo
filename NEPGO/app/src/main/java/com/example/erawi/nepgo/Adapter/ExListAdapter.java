package com.example.erawi.nepgo.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.erawi.nepgo.R;

import java.util.List;
import java.util.Map;


/**
 * Created by Erawi on 22.3.2017.
 */

public class ExListAdapter extends BaseExpandableListAdapter {

    Context context;
    private List<String> list;
    private Map<String, List<String>> info;

    public ExListAdapter(Context context, List<String> list, Map<String, List<String>> info) {
        this.context = context;
        this.list = list;
        this.info = info;
    }

    @Override
    public int getGroupCount() {
        return list.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return info.get(list.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return list.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return info.get(list.get(groupPosition)).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        String list =(String)getGroup(groupPosition);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);

        if(convertView == null){
            convertView = inflater.inflate(R.layout.list_parent, null);
        }

        ImageView img = (ImageView)convertView.findViewById(R.id.itemicon);
        TextView txtParent = (TextView)convertView.findViewById(R.id.txtParent);
        txtParent.setText(list);

        if(txtParent.getText().equals("Who are we?")||txtParent.getText().equals("हामी को हौँ ?")){
            img.setImageResource(R.drawable.ic_who);
        }
        if(txtParent.getText().equals("How does NEPGO work?")||txtParent.getText().equals("हामी कसरी काम गर्छौ ?")){
            img.setImageResource(R.drawable.ic_how);
        }
        if(txtParent.getText().equals("You are an Educational Institute?")||txtParent.getText().equals("तपाईं शैक्षिक संस्थान हुनुहुन्छ ?")){
            img.setImageResource(R.drawable.ic_edu);
        }
        if(txtParent.getText().equals("You are a Business / Company?")||txtParent.getText().equals("तपाईं उद्योग, व्यवसाय वा कम्पनी हुनुहुन्छ ?")){
            img.setImageResource(R.drawable.ic_business);
        }
        if(txtParent.getText().equals("You are an Expert / Specialist?")||txtParent.getText().equals("तपाईं विशेषज्ञ हुनुहुन्छ ?")){
            img.setImageResource(R.drawable.ic_expert);
        }
        if(txtParent.getText().equals("You are a profit or nonprofit Organization?")||txtParent.getText().equals("तपाईं नाफामूलक वा गैर नाफामूलक संस्था हुनुहुन्छ ?")){
            img.setImageResource(R.drawable.ic_organization);
        }
        if(txtParent.getText().equals("You are a Student Volunteer?")||txtParent.getText().equals("तपाईं बिद्यार्थी हुनुहुन्छ ?")){
            img.setImageResource(R.drawable.ic_student);
        }
        if(txtParent.getText().equals("You are a General Visitor?")||txtParent.getText().equals("तपाई सामान्य रुपमा सहभागी हुन चाहनुहुन्छ ?")){
            img.setImageResource(R.drawable.ic_visitor);
        }

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String info =(String)getChild(groupPosition, childPosition);

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_child, null);
        }

        TextView txtChild = (TextView)convertView.findViewById(R.id.txtChild);
        txtChild.setText(info);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

