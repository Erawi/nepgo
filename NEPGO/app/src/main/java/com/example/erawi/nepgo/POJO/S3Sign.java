package com.example.erawi.nepgo.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Erawi on 24.8.2017.
 */

public class S3Sign {

    @SerializedName("signedRequest")
    @Expose
    private String signedRequest;
    @SerializedName("url")
    @Expose
    private String url;

    public String getSignedRequest() {
        return signedRequest;
    }

    public void setSignedRequest(String signedRequest) {
        this.signedRequest = signedRequest;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
