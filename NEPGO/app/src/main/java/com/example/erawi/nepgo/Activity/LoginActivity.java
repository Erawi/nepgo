package com.example.erawi.nepgo.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.LoginRequest;
import com.example.erawi.nepgo.POJO.LoginResponse;
import com.example.erawi.nepgo.R;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText mEmailView ,mPasswordView;
    private View mProgressView;
    private View focusView = null;
    private String email;
    private String password;
    private Button signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_login);

        // Initialize elements
        mProgressView = findViewById(R.id.login_progress);
        mEmailView = (EditText)findViewById(R.id.emailField);
        mPasswordView = (EditText) findViewById(R.id.passwordField);
        signInButton = (Button)findViewById(R.id.signinB);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });
    }

    // Moving between activities
    public void launchRegisterActivity(View v){
        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
    }
    public void launchAfterLoginActivity(View v){
        startActivity(new Intent(LoginActivity.this, NewsfeedActivity.class));
    }

    // Method for keyboard hiding
    private void hideSoftKeyboard(){
        if(getCurrentFocus()!=null && getCurrentFocus() instanceof EditText){
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mPasswordView.getWindowToken(), 0);
        }
    }

    // Attempt login, hide keyboard & enable progress icon
    private void attemptLogin(){
        boolean mCancel = this.loginValidation();
        if (mCancel) {
            focusView.requestFocus();
        } else {
            loginProcessWithRetrofit();
            hideSoftKeyboard();
            showProgress(true);
        }
    }

    private boolean loginValidation() {
        // Reset errors
        mEmailView.setError(null);
        mPasswordView.setError(null);
        boolean cancel = false;

        // Store values at the time of the login attempt
        email = mEmailView.getText().toString();
        password = mPasswordView.getText().toString();

        // Check for a valid password, if the user entered one
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }
        return cancel;
    }

    // Conditions for valid email/password
    private boolean isEmailValid(String email) { return email.contains("@"); }
    private boolean isPasswordValid(String password) { return password.length() > 4; }

    // Progressbar animation
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    // Login method using retrofit
    private void loginProcessWithRetrofit(){
        LoginRequest loginRequest = new LoginRequest();

        // Set email & password according to the LoginRequest model to prepare for sending
        loginRequest.setEmail(mEmailView.getText().toString());
        loginRequest.setPassword(mPasswordView.getText().toString());

        // Make a retrofit call using an instance of RestClient
        ApiInterface service = RestClient.getClient();
        Call<LoginResponse> loginResponseCall = service.loginUser(loginRequest);
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                // Get statuscode from response
                int statuscode = response.code();

                // Get whole body of the response
                LoginResponse loginResponse = response.body();
                showProgress(false);

                if(statuscode == 200){
                    // Save user info to a sharedpref file on login
                    SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = sharedPref.edit();
                    edit.putString("token", loginResponse.getToken());
                    edit.putString("id", loginResponse.getUser().getId());
                    edit.putString("name", loginResponse.getUser().getName());
                    edit.putString("city", loginResponse.getUser().getCity());
                    edit.putString("country", loginResponse.getUser().getCountry());
                    edit.putString("extraInfo", loginResponse.getUser().getExtraInfo());
                    edit.putString("educations", loginResponse.getUser().getEducations().toString());
                    edit.putString("experiences", loginResponse.getUser().getExperiences().toString());
                    edit.putString("profession", loginResponse.getUser().getProfession());
                    edit.putString("sectors", loginResponse.getUser().getSectors().toString());
                    edit.putString("skills", loginResponse.getUser().getSkills().toString());
                    edit.putString("roles", loginResponse.getUser().getRoles().toString());
                    edit.apply();

                    Intent intent = new Intent(LoginActivity.this, NewsfeedActivity.class);
                    startActivity(intent);

                    // Display a notification on login
                    NotificationCompat.Builder login_notification = new NotificationCompat.Builder(getApplicationContext());
                    login_notification.setSmallIcon(R.drawable.notification_icon1);
                    login_notification.setContentTitle("Welcome!");
                    login_notification.setContentText("You have successfully logged in Nepgo.");

                    final int notificationID = 111;
                    NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    // notificationID allows you to update the notification later on.
                    mNotificationManager.notify(notificationID, login_notification.build());

                    SharedPreferences pref = getSharedPreferences("SharedValue", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("sptoken", "not updated");
                    editor.commit();

                    Toast.makeText(LoginActivity.this, "Welcome " + loginResponse.getUser().getName(), Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(LoginActivity.this, "Wrong email/password", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Debug
                Log.d("LoginActivity", "OnFailure: " + t.getMessage());
                Toast.makeText(LoginActivity.this, "Check your internet connection", Toast.LENGTH_SHORT).show();
                showProgress(false);
            }
        });
    }
}
