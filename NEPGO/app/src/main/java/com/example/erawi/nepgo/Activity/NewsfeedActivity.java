package com.example.erawi.nepgo.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.erawi.nepgo.Adapter.RecyclerViewAdapter;
import com.example.erawi.nepgo.Adapter.RecyclerViewAdapterReports;
import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.Timeline;
import com.example.erawi.nepgo.R;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsfeedActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private String id, name, address, city, country, email, extraInfo, locale, profession, role, educations, experiences, skills, sectors, tempEmail, tempPass, token;
    private ApiInterface service;
    private ListView postsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_newsfeed);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());


        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Button profileButton = (Button)findViewById(R.id.profileButton);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchProfileActivity();

                final SharedPreferences pref1 = getSharedPreferences("SharedValue", MODE_PRIVATE);
                String spvalue = pref1.getString("sptoken", "");

                if(!spvalue.equalsIgnoreCase("updated"))
                {
                    Toast toast = Toast.makeText(getApplicationContext(), (getResources().getString(R.string.update_profile)), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP, 0, 200);
                    View view = toast.getView();
                    toast.setView(view);
                    toast.show();
                }
            }
        });
        Button blogButton = (Button)findViewById(R.id.blogButton);
        blogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NewsfeedActivity.this, BlogActivity.class));
            }
        });
    }

    private void launchProfileActivity(){
        startActivity(new Intent(NewsfeedActivity.this, ProfileActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_after_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private LinearLayoutManager layoutManager;
        private Button newPost;
        private ImageButton likePost;
        private View mProgressView;

        // Progressbar animation
        @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
        private void showProgress(final boolean show) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
                int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                mProgressView.animate().setDuration(shortAnimTime).alpha(
                        show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                    }
                });
            } else {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        }
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);

            newPost = (Button)getActivity().findViewById(R.id.postAssignmentButton);
            likePost = (ImageButton)getActivity().findViewById(R.id.btnLike);

            newPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getActivity(), CreatePostActivity.class));
                }
            });
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            mProgressView = getActivity().findViewById(R.id.progressBar2);
            if(getArguments().getInt(ARG_SECTION_NUMBER) == 1)
            {
                View rootView = inflater.inflate(R.layout.fragment_sub_page01, container, false);

                try {
                    ApiInterface service = RestClient.getClient();
                    Call<List<Timeline>> call = service.loadTimeline();

                    showProgress(true);

                    call.enqueue(new Callback<List<Timeline>>() {
                        @Override
                        public void onResponse(Call<List<Timeline>> call, Response<List<Timeline>> response) {
                            List<Timeline> postList = response.body();

                            // Sort posts by date
                            Collections.sort(postList, new Comparator<Timeline>() {
                                @Override
                                public int compare(Timeline o1, Timeline o2) {
                                    return o2.getCreated().compareTo(o1.getCreated());
                                }
                            });

                            layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setAutoMeasureEnabled(false);

                            RecyclerView recyclerView = (RecyclerView)getActivity().findViewById(R.id.recycler_view);
                            recyclerView.setLayoutManager(layoutManager);

                            RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(postList, getContext());
                            recyclerView.setAdapter(recyclerViewAdapter);

                            showProgress(false);
                        }

                        @Override
                        public void onFailure(Call<List<Timeline>> call, Throwable t) {
                            Log.d("Newsfeed: ", t.getMessage());
                            showProgress(false);
                        }
                    });

                } catch (Exception e) {
                    Log.d("Newsfeed: ", e.getMessage());
                    showProgress(false);
                }
                return rootView;
            }
            if(getArguments().getInt(ARG_SECTION_NUMBER) == 2)
            {
                View rootView = inflater.inflate(R.layout.fragment_sub_page02, container, false);
                try {
                    ApiInterface service = RestClient.getClient();
                    Call<List<Timeline>> call = service.loadTimeline();

                    showProgress(true);

                    call.enqueue(new Callback<List<Timeline>>() {
                        @Override
                        public void onResponse(Call<List<Timeline>> call, Response<List<Timeline>> response) {
                            List<Timeline> postList = response.body();

                            // Sort posts by date
                            Collections.sort(postList, new Comparator<Timeline>() {
                                @Override
                                public int compare(Timeline o1, Timeline o2) {
                                    return o2.getCreated().compareTo(o1.getCreated());
                                }
                            });

                            layoutManager = new LinearLayoutManager(getActivity());
                            layoutManager.setAutoMeasureEnabled(false);

                            RecyclerView recyclerView = (RecyclerView)getActivity().findViewById(R.id.recycler_view_reports);
                            recyclerView.setLayoutManager(layoutManager);

                            RecyclerViewAdapterReports recyclerViewAdapterReports = new RecyclerViewAdapterReports(postList, getContext());
                            recyclerView.setAdapter(recyclerViewAdapterReports);

                            showProgress(false);
                        }

                        @Override
                        public void onFailure(Call<List<Timeline>> call, Throwable t) {
                            Log.d("Newsfeed: ", t.getMessage());
                            showProgress(false);
                        }
                    });

                } catch (Exception e) {
                    Log.d("Newsfeed: ", e.getMessage());
                    showProgress(false);
                }
                return rootView;
            }
            else if(getArguments().getInt(ARG_SECTION_NUMBER) == 3)
            {
                View rootView = inflater.inflate(R.layout.fragment_sub_page03, container, false);

                final TextView infoText = (TextView) rootView.findViewById(R.id.text_info);

                Button b1 = (Button) rootView.findViewById(R.id.button1);
                b1.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add1)));
                    }
                });
                Button b2 = (Button) rootView.findViewById(R.id.button2);
                b2.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add2)));
                    }
                });
                Button b3 = (Button) rootView.findViewById(R.id.button3);
                b3.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add3)));
                    }
                });
                Button b4 = (Button) rootView.findViewById(R.id.button4);
                b4.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add4)));
                    }
                });
                Button b5 = (Button) rootView.findViewById(R.id.button5);
                b5.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add5)));
                    }
                });
                Button b6 = (Button) rootView.findViewById(R.id.button6);
                b6.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add6)));
                    }
                });
                Button b7 = (Button) rootView.findViewById(R.id.button7);
                b7.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add7)));
                    }
                });
                Button b8 = (Button) rootView.findViewById(R.id.button8);
                b8.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v)
                    {
                        infoText.setText((getResources().getString(R.string.add8)));
                    }
                });

                return rootView;
            }
            return null;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return (getResources().getString(R.string.newsfeed));
                case 1:
                    return (getResources().getString(R.string.reports));
                case 2:
                    return (getResources().getString(R.string.nepgo));
            }
            return null;
        }
    }
}
