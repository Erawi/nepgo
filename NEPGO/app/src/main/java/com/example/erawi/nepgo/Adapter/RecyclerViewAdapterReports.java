package com.example.erawi.nepgo.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.erawi.nepgo.Activity.NewsfeedActivity;
import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.Comment;
import com.example.erawi.nepgo.POJO.Post;
import com.example.erawi.nepgo.POJO.Timeline;
import com.example.erawi.nepgo.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Erawi on 12.6.2017.
 */

public class RecyclerViewAdapterReports extends RecyclerView.Adapter<ReportsAdapter> {

    private List<Timeline> item;
    private Context context;
    private String contentType;
    private String imageURL;

    public RecyclerViewAdapterReports(List<Timeline> list, Context context){
        this.item = list;
        this.context = context;
    }

    @Override
    public ReportsAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.timelinerow, parent, false);

        ReportsAdapter reportsAdapter = new ReportsAdapter(layoutView);

        return reportsAdapter;
    }

    @Override
    public void onBindViewHolder(final ReportsAdapter holder, final int position) {
        // Get user token from sharedprefs
        SharedPreferences sharedPref = context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        final String token = sharedPref.getString("token", "");
        final String userRole = sharedPref.getString("roles", "");
        final String userId = sharedPref.getString("id", "");
        final String post_id = item.get(position).getId();

        // Set body for new comment payload
        // TODO: handle cases where user has more than one role
        // TODO: get text from items specific edittext

        holder.name.setText(item.get(position).getUser().getName());
        holder.sector.setText(item.get(position).getSector());
        holder.date.setText(item.get(position).getCreated().substring(0, 10));
        holder.title.setText(item.get(position).getTitle());
        holder.desc.setText(item.get(position).getDescription());
        if(item.get(position).getComments().size() > 0){
            for(int i = 0; i < item.get(position).getComments().size(); i++){
                holder.commentName.append(item.get(position).getComments().get(i).getUser().getName() + ": "  + "\n");
                holder.comment.append(item.get(position).getComments().get(i).getText() + "\n");
            }
        }
        holder.likes.setText(Integer.toString(item.get(position).getLikes().size()));
        holder.commentCount.setText(Integer.toString(item.get(position).getComments().size()));
        if(item.get(position).getImages().size() > 0){

            imageURL = TextUtils.join(", ", item.get(position).getImages());
            holder.image.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(imageURL)
                    .into(holder.image);
        }

        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    ApiInterface service = RestClient.getClient();
                    Call<Post> call = service.likePost(post_id, token);

                    call.enqueue(new Callback<Post>() {
                        @Override
                        public void onResponse(Call<Post> call, Response<Post> response) {
                            int statusCode = response.code();

                            if(statusCode == 200){
                                //TODO: update like count after succesful like
                                Toast.makeText(context, "Liked!", Toast.LENGTH_SHORT).show();
                                // Refresh activity to show changes
                                ((NewsfeedActivity)context).recreate();
                            }
                            else{
                                Toast.makeText(context, "A problem occurred." + " " + statusCode, Toast.LENGTH_SHORT).show();
                                Log.d("Like error: ", String.valueOf(response.errorBody()));
                            }
                        }

                        @Override
                        public void onFailure(Call<Post> call, Throwable t) {
                            Log.d("RecyclerView", "OnFailure: " + t.getMessage());
                            Toast.makeText(context, "Check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    Log.d("Like: ", e.getMessage());
                }
            }
        });

        holder.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    SharedPreferences sharedPref = context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    Comment comment = new Comment();
                    comment.setText(holder.commentLine.getText().toString());
                    comment.setRole(sharedPref.getString("roles", "").replaceAll("\\]", "").replaceAll("\\[", ""));

                    ApiInterface service = RestClient.getClient();
                    Call<Comment> call = service.commentPost(post_id, token, comment);

                    call.enqueue(new Callback<Comment>() {
                        @Override
                        public void onResponse(Call<Comment> call, Response<Comment> response) {
                            int statusCode = response.code();

                            if(statusCode == 200){
                                Toast.makeText(context, "Commented!" , Toast.LENGTH_SHORT).show();
                                // Refresh activity to display changes
                                ((NewsfeedActivity)context).recreate();

                            }
                            else{
                                Toast.makeText(context, "A problem occurred: " + statusCode + " ", Toast.LENGTH_SHORT).show();
                                Log.d("Comment error: ", String.valueOf(response.errorBody()));
                            }
                        }

                        @Override
                        public void onFailure(Call<Comment> call, Throwable t) {
                            Log.d("RecyclerView", "OnFailure: " + t.getMessage());
                            Toast.makeText(context, "Check your internet connection", Toast.LENGTH_SHORT).show();
                        }
                    });


                } catch (Exception e){
                    Log.d("Comment send: ", e.getMessage());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return item.size();
    }
}
