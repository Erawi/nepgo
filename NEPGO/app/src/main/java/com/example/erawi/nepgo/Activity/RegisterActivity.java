package com.example.erawi.nepgo.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.Register;
import com.example.erawi.nepgo.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    private EditText name, email, password, confirmPassword;
    private Spinner countrySpinner, citySpinner;
    private String country, city;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_register);

        // Initialize elements
        name = (EditText)findViewById(R.id.nameET);
        email = (EditText)findViewById(R.id.emailET);
        password = (EditText)findViewById(R.id.passwordET);
        confirmPassword = (EditText)findViewById(R.id.confirmpasswordET);
        countrySpinner = (Spinner)findViewById(R.id.countrySpinner);
        citySpinner = (Spinner)findViewById(R.id.citySpinner);
        registerButton = (Button)findViewById(R.id.registerButton);

        // Inflate spinner menus
        ArrayAdapter countryAdapter = ArrayAdapter.createFromResource(this, R.array.country_options, android.R.layout.simple_spinner_item);
        final ArrayAdapter cityAdapter = ArrayAdapter.createFromResource(this, R.array.city_options, android.R.layout.simple_spinner_item);
        countrySpinner.setAdapter(countryAdapter);
        citySpinner.setAdapter(cityAdapter);
        countrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView countryText = (TextView) view;
                country = countryText.getText().toString();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        citySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView cityText = (TextView) view;
                city = cityText.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptRegistration();
            }
        });
    }

    private void attemptRegistration(){
        final ApiInterface service = RestClient.getClient();
        Register register = new Register();

        if(password.getText().equals(confirmPassword.getText())){
            Toast.makeText(RegisterActivity.this, "Passwords do not match", Toast.LENGTH_SHORT).show();
            return;
        }

        //TODO: giving bad request when sending null fields that are NOT required

        // Get variables for sending
        register.setName(name.getText().toString());
        register.setEmail(email.getText().toString());
        register.setCountry(country);
        register.setCity(city);
        register.setPassword(password.getText().toString());
        register.setConfirm_password(confirmPassword.getText().toString());

        Call<Register> registerCall = service.registerUser(register);

        registerCall.enqueue(new Callback<Register>() {
            @Override
            public void onResponse(Call<Register> call, Response<Register> response) {
                int statusCode = response.code();
                String message = response.message();

                if (statusCode == 200) {
                    Toast.makeText(RegisterActivity.this, "Success!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));

                } else {
                    //Error handling
                    BufferedReader reader = null;
                    StringBuilder sb = new StringBuilder();
                    reader = new BufferedReader(new InputStreamReader(response.errorBody().byteStream()));
                    String line;
                    try {
                        while ((line = reader.readLine()) != null) {
                            sb.append(line);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String finalError = sb.toString();

                    if(finalError.contains("User already exists with the given email")){
                        Toast.makeText(RegisterActivity.this, "User already exists with the given email", Toast.LENGTH_LONG).show();
                    }
                    else if(finalError.contains("Mandatory fields are missing")){
                        Toast.makeText(RegisterActivity.this, "Mandatory fields are missing", Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(RegisterActivity.this, "Registration failed: " + message + " " + statusCode, Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<Register> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
            }
        });
    }
}
