package com.example.erawi.nepgo.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.Register;
import com.example.erawi.nepgo.POJO.S3Sign;
import com.example.erawi.nepgo.POJO.User;
import com.example.erawi.nepgo.R;
import com.ipaulpro.afilechooser.utils.FileUtils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditProfileActivity extends AppCompatActivity{

    private CheckBox education, engineering, health_services, social_services, sports, academia, company, experts, organization, volunteer;
    private EditText profession, educations, extra_info, skills;
    private Button updateButton, uploadImageButton;
    private String userId, userToken, academiaString, companyString, expertsString, organizationString, volunteerString,
            educationString, engineeringString, health_servicesString, social_servicesString, sportsString, roleString, mimeType, uploadUri, previewUri, baseURL, finalUploadUri, decodedUri;
    private ArrayList<String> sectorStrings = new ArrayList<>();
    private ArrayList<String> roleStrings = new ArrayList<>();
    private List<String> edu = new ArrayList<>();
    private List<String> skillz = new ArrayList<>();
    private File originalFile;
    private int PICK_FILE_FROM_DEVICE = 1;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_edit_profile);

        academiaString = "academia";
        companyString = "company";
        expertsString = "experts";
        organizationString = "organization";
        volunteerString = "volunteer";
        educationString = "Education";
        engineeringString = "Engineering";
        health_servicesString = "Health services";
        social_servicesString = "Social services";
        sportsString = "sports";

        //Get user info from sharedpref
        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        userToken = sharedPref.getString("token", "");
        userId = sharedPref.getString("id", "");
        baseURL = "https://test-prata.s3.amazonaws.com/";

        updateButton = (Button)findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uri != null){
                    getSignedUri(mimeType);
                }
                else{
                    attemptUpdate();
                }

                SharedPreferences pref = getSharedPreferences("SharedValue", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("sptoken", "updated");
                editor.commit();
            }
        });

        uploadImageButton = (Button)findViewById(R.id.uploadImgBtn);
        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: HANDLE SAMSUNG DEVICES
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, PICK_FILE_FROM_DEVICE);
            }
        });

        education = (CheckBox)findViewById(R.id.education);
        engineering = (CheckBox)findViewById(R.id.engineering);
        health_services = (CheckBox)findViewById(R.id.health);
        social_services = (CheckBox)findViewById(R.id.social);
        sports = (CheckBox)findViewById(R.id.sports);
        academia = (CheckBox)findViewById(R.id.academia);
        company = (CheckBox)findViewById(R.id.company);
        organization = (CheckBox)findViewById(R.id.organization);
        experts = (CheckBox)findViewById(R.id.experts);
        volunteer = (CheckBox)findViewById(R.id.volunteer);
        profession = (EditText)findViewById(R.id.professionET);
        educations = (EditText)findViewById(R.id.eduET);
        skills = (EditText)findViewById(R.id.skillsET);
        extra_info = (EditText)findViewById(R.id.extraET);
    }

    public void selectItem(View view){
        boolean checked = ((CheckBox) view).isChecked();
        switch (view.getId()){
            case R.id.education:
                if(checked){
                    sectorStrings.add(educationString);
                } else {
                    sectorStrings.remove(educationString);
                }
                break;
            case R.id.engineering:
                if(checked){
                    sectorStrings.add(engineeringString);
                } else {
                    sectorStrings.remove(engineeringString);
                }
                break;
            case R.id.health:
                if(checked){
                    sectorStrings.add(health_servicesString);
                } else {
                    sectorStrings.remove(health_servicesString);
                }
                break;
            case R.id.social:
                if(checked){
                    sectorStrings.add(social_servicesString);
                } else {
                    sectorStrings.remove(social_servicesString);
                }
                break;
            case R.id.sports:
                if(checked){
                    sectorStrings.add(sportsString);
                } else {
                    sectorStrings.remove(sportsString);
                }
                break;
            case R.id.academia:
                if(checked){
                    roleStrings.add(academiaString);
                } else {
                    roleStrings.remove(academiaString);
                }
                break;
            case R.id.company:
                if(checked){
                    roleStrings.add(companyString);
                } else {
                    roleStrings.remove(companyString);
                }
                break;
            case R.id.experts:
                if(checked){
                    roleStrings.add(expertsString);
                } else {
                    roleStrings.remove(expertsString);
                }
                break;
            case R.id.organization:
                if(checked){
                    roleStrings.add(organizationString);
                } else {
                    roleStrings.remove(organizationString);
                }
                break;
            case R.id.volunteer:
                if(checked){
                    roleStrings.add(volunteerString);
                } else {
                    roleStrings.remove(volunteerString);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_FILE_FROM_DEVICE && resultCode == RESULT_OK && data != null && data.getData() != null){
            uri = data.getData();
            Log.d("uri: ", uri.toString());
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
            Log.d("mimetype: ", mimeType);
            //getSignedUri(mimetype);
        }
    }

    public void getSignedUri(final String fileType){
        ApiInterface service = RestClient.getClient();
        Call<S3Sign> call = service.getSignedUrl(userToken, fileType);
        call.enqueue(new Callback<S3Sign>() {
            @Override
            public void onResponse(Call<S3Sign> call, Response<S3Sign> response) {
                int statusCode = response.code();
                S3Sign s3Sign = response.body();
                uploadUri = s3Sign.getSignedRequest();
                previewUri = s3Sign.getUrl();

                if(statusCode == 200){
                    Log.d("SignedRequest: ", uploadUri);
                    Log.d("URL: ", previewUri);
                    finalUploadUri = uploadUri.replace("https://test-prata.s3.amazonaws.com/", "");
                    try{
                        decodedUri = URLDecoder.decode(finalUploadUri, "UTF-8");
                    }catch (UnsupportedEncodingException e){
                        e.printStackTrace();
                    }
                    uploadFile(uri, fileType);
                }
                else {
                    Toast.makeText(EditProfileActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<S3Sign> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });
    }

    public void uploadFile(Uri fileUri, String fileType) {
        originalFile = FileUtils.getFile(this, fileUri);
        String pathToFile = originalFile.getPath();
        File fileToBody = new File(pathToFile);

        RequestBody file = RequestBody.create(MediaType.parse(fileType), fileToBody);

        // Add logging into retrofit
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.interceptors().add(logging);

        //Create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build());
        Retrofit retrofit = builder.build();
        ApiInterface client = retrofit.create(ApiInterface.class);

        Log.d("uploadURI: ", finalUploadUri);

        Call<ResponseBody> call = client.uploadImage(finalUploadUri, file);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if(statusCode == 200){
                    Toast.makeText(EditProfileActivity.this, "Uploaded file successfully", Toast.LENGTH_SHORT).show();
                    // attempt to post if succesful response
                    attemptUpdate();
                }
                else{
                    Toast.makeText(EditProfileActivity.this, "Failure " + statusCode, Toast.LENGTH_SHORT).show();
                    Log.d("Failure: ", response.message());
                    Log.d("Failure: ", response.headers().toString());
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(EditProfileActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                Log.d("Failure: ", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    private void attemptUpdate(){

        ApiInterface service = RestClient.getClient();

        edu.add(educations.getText().toString());
        skillz.add(skills.getText().toString());

        User user = new User();

        //TODO: FIX EXTRA INFO
        // Get variables for sending
        if(!profession.getText().toString().matches("")){
            user.setProfession(profession.getText().toString());
        }
        if(!educations.getText().toString().matches("")){
            user.setEducations(edu);
        }
        if(!skillz.isEmpty()){
            user.setSkills(skillz);
        }
        if(!extra_info.getText().toString().matches("")){
            user.setExtraInfo(extra_info.getText().toString());
        }
        if(!roleStrings.isEmpty()){
            user.setRoles(roleStrings);
        }
        if(!sectorStrings.isEmpty()){
            user.setSectors(sectorStrings);
        }
        if(previewUri != null){
            user.setImage(previewUri);
        }

        final Call<User> updateCall = service.updateUser(userId, userToken, user);

        updateCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                int statusCode = response.code();

                if(statusCode == 200){
                    Toast.makeText(EditProfileActivity.this, "Success!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(EditProfileActivity.this, ProfileActivity.class));
                }
                else{
                    Toast.makeText(EditProfileActivity.this, "Error!: " + statusCode + " " + response.message(), Toast.LENGTH_LONG).show();
                    Log.d("failure: ", response.message());
                    Log.d("id, token:  ", userId + " - " + userToken);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(EditProfileActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                Log.d("Failure", t.getMessage());
            }
        });
    }
}
