package com.example.erawi.nepgo.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.Post;
import com.example.erawi.nepgo.POJO.S3Sign;
import com.example.erawi.nepgo.R;
import com.ipaulpro.afilechooser.utils.FileUtils;
import com.kosalgeek.android.photoutil.GalleryPhoto;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CreatePostActivity extends AppCompatActivity {

    private Button newPost, startDate, endDate, chooseFile;
    private EditText titleET, descET;
    private String userToken, userId, uploadUri, previewUri, baseURL, finalUploadUri, decodedUri, sector, role, mimeType;
    private Spinner sectorSpinner, roleSpinner;
    private TextView tvStartDate, tvEndDate;
    private DatePickerDialog.OnDateSetListener startDateSetListener, endDateSetListener;
    private File originalFile;
    private int PICK_FILE_FROM_DEVICE = 1;
    private Uri uri;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_create_post);

        // Initialize UI elements
        titleET = (EditText)findViewById(R.id.postTitleET);
        descET = (EditText)findViewById(R.id.postDescET);
        newPost = (Button)findViewById(R.id.newPostButton);
        sectorSpinner = (Spinner)findViewById(R.id.sectorSpinner);
        roleSpinner = (Spinner)findViewById(R.id.roleSpinner);
        startDate = (Button)findViewById(R.id.startDateBtn);
        endDate = (Button)findViewById(R.id.endDateBtn);
        tvStartDate = (TextView)findViewById(R.id.tvStartDate);
        tvEndDate = (TextView)findViewById(R.id.tvEndDate);
        chooseFile = (Button)findViewById(R.id.fileChooseButton);

        // Get user token from sharedpref
        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        userToken = sharedPref.getString("token", "");
        userId = sharedPref.getString("id", "");
        baseURL = "https://test-prata.s3.amazonaws.com/";

        // Add adapter for sector/role spinner and inflate them
        ArrayAdapter sectorAdapter = ArrayAdapter.createFromResource(this, R.array.sector_array, android.R.layout.simple_spinner_item);
        ArrayAdapter roleAdapter = ArrayAdapter.createFromResource(this, R.array.role_array, android.R.layout.simple_spinner_item);
        sectorSpinner.setAdapter(sectorAdapter);
        roleSpinner.setAdapter(roleAdapter);
        sectorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView sectorText = (TextView) view;
                sector = sectorText.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });
        roleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView roleText = (TextView) view;
                role = roleText.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        newPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSignedUri(mimeType);
            }
        });

        // Get date by using DatePicker
        startDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int startYear = cal.get(Calendar.YEAR);
                int startMonth = cal.get(Calendar.MONTH);
                int startDay = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(CreatePostActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, startDateSetListener, startYear, startMonth, startDay);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        startDateSetListener = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker datePicker, int startYear, int startMonth, int startDay){
                startMonth = startMonth + 1;
                String startDate = startYear + "/" + startMonth + "/" + startDay;
                tvStartDate.setText(startDate);
            }
        };
        endDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int endYear = cal.get(Calendar.YEAR);
                int endMonth = cal.get(Calendar.MONTH);
                int endDay = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(CreatePostActivity.this, android.R.style.Theme_Holo_Light_Dialog_MinWidth, endDateSetListener, endYear, endMonth, endDay);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });
        endDateSetListener = new DatePickerDialog.OnDateSetListener(){
            @Override
            public void onDateSet(DatePicker datePicker, int endYear, int endMonth, int endDay){
                endMonth = endMonth + 1;
                String endDate = endYear + "/" + endMonth + "/" + endDay;
                tvEndDate.setText(endDate);
            }
        };

        chooseFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: HANDLE SAMSUNG DEVICES
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                startActivityForResult(intent, PICK_FILE_FROM_DEVICE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_FILE_FROM_DEVICE && resultCode == RESULT_OK && data != null && data.getData() != null){
            uri = data.getData();
            Log.d("uri: ", uri.toString());
            ContentResolver cr = getContentResolver();
            mimeType = cr.getType(uri);
            Log.d("mimetype: ", mimeType);
            //getSignedUri(mimetype);
        }
    }

    public void getSignedUri(final String fileType){
        ApiInterface service = RestClient.getClient();
        Call<S3Sign> call = service.getSignedUrl(userToken, fileType);
        call.enqueue(new Callback<S3Sign>() {
            @Override
            public void onResponse(Call<S3Sign> call, Response<S3Sign> response) {
                int statusCode = response.code();
                S3Sign s3Sign = response.body();
                uploadUri = s3Sign.getSignedRequest();
                previewUri = s3Sign.getUrl();

                if(statusCode == 200){
                    Log.d("SignedRequest: ", uploadUri);
                    Log.d("URL: ", previewUri);
                    finalUploadUri = uploadUri.replace("https://test-prata.s3.amazonaws.com/", "");
                    try{
                        decodedUri = URLDecoder.decode(finalUploadUri, "UTF-8");
                    }catch (UnsupportedEncodingException e){
                        e.printStackTrace();
                    }
                    uploadFile(uri, fileType);
                }
                else {
                    Toast.makeText(CreatePostActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<S3Sign> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });
    }

    public void uploadFile(Uri fileUri, String fileType) {
        originalFile = FileUtils.getFile(this, fileUri);
        String pathToFile = originalFile.getPath();
        File fileToBody = new File(pathToFile);

        RequestBody file = RequestBody.create(MediaType.parse(fileType), fileToBody);

        // Add logging into retrofit
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.interceptors().add(logging);

        //Create retrofit instance
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build());
        Retrofit retrofit = builder.build();
        ApiInterface client = retrofit.create(ApiInterface.class);

        Log.d("uploadURI: ", finalUploadUri);

        Call<ResponseBody> call = client.uploadImage(finalUploadUri, file);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if(statusCode == 200){
                    Toast.makeText(CreatePostActivity.this, "Uploaded file successfully", Toast.LENGTH_SHORT).show();
                    // attempt to post if succesful response
                    attemptPost();
                }
                else{
                    Toast.makeText(CreatePostActivity.this, "Failure " + statusCode, Toast.LENGTH_SHORT).show();
                    Log.d("Failure: ", response.message());
                    Log.d("Failure: ", response.headers().toString());
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(CreatePostActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                Log.d("Failure: ", t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public void attemptPost(){
        ApiInterface service = RestClient.getClient();
        Post post = new Post();
        List<String> fileList = new ArrayList<String>();
        fileList.add(previewUri);

        Call<Post> call = service.createNewPost(userToken, post);

        post.setTitle(titleET.getText().toString());
        post.setDescription(descET.getText().toString());
        post.setSector(sector);
        post.setRole(role);
        post.setImages(fileList);

        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                int statusCode = response.code();

                if(statusCode == 200){
                    Toast.makeText(CreatePostActivity.this, "Success!", Toast.LENGTH_LONG).show();
                    startActivity(new Intent(CreatePostActivity.this, NewsfeedActivity.class));
                }
                else{
                    Toast.makeText(CreatePostActivity.this, "Error!: " + statusCode + " " + response.message(), Toast.LENGTH_LONG).show();
                    Log.d("failure: ", response.message());
                }
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {
                Log.d("Timeline: ", t.getMessage());
            }
        });
    }
}
