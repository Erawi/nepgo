package com.example.erawi.nepgo.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.erawi.nepgo.Helper.RestClient;
import com.example.erawi.nepgo.Interface.ApiInterface;
import com.example.erawi.nepgo.POJO.Register;
import com.example.erawi.nepgo.POJO.User;
import com.example.erawi.nepgo.R;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private String id, token;
    private View mProgressView;
    private ImageView profileImage;
    private TextView profession, educations, skills, city, notes, name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final SharedPreferences lng_pref = getSharedPreferences("lng_sp", MODE_PRIVATE);
        String language = lng_pref.getString("language", "");

        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        setContentView(R.layout.activity_profile);

        mProgressView = findViewById(R.id.progressBar);
        name = (TextView)findViewById(R.id.name);
        profession = (TextView)findViewById(R.id.tvProfession);
        educations = (TextView)findViewById(R.id.tvEducation);
        skills = (TextView)findViewById(R.id.tvSkills);
        city = (TextView)findViewById(R.id.tvCity);
        notes = (TextView)findViewById(R.id.tvNotes);
        profileImage = (ImageView)findViewById(R.id.profileImage);
        showProgress(false);

        // Get user token from sharedpref
        SharedPreferences sharedPref = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        id = sharedPref.getString("id", "");
        token = sharedPref.getString("token", "");

        // Make a retrofit call using an instance of RestClient
        ApiInterface service = RestClient.getClient();
        Call<List<User>> call = service.loadUser(id);
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                int statusCode = response.code();
                List<User> user = response.body();

                showProgress(true);
                if(statusCode == 200){
                    Toast.makeText(ProfileActivity.this, "Loaded user data succesfully!", Toast.LENGTH_SHORT).show();

                    name.setText(user.get(0).getName());
                    Glide.with(getApplicationContext()).load(user.get(0).getImage()).into(profileImage);
                    educations.setText(user.get(0).getEducations().toString().replaceAll("\\]", "").replaceAll("\\[", ""));
                    skills.setText(user.get(0).getSkills().toString().replaceAll("\\]", "").replaceAll("\\[", ""));
                    city.setText(user.get(0).getCity());
                    profession.setText(user.get(0).getProfession());
                    notes.setText(user.get(0).getExtraInfo());

                    showProgress(false);
                }
                else{
                    Toast.makeText(ProfileActivity.this, "Problem loading user data: " + response.message(), Toast.LENGTH_LONG).show();
                    showProgress(false);
                }
            }
            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.d("Failure", t.getMessage());
            }
        });
    }

    public void launchProfileEditActivity(View launchRegisterFinishActivity){
        Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
        startActivity(intent);
    }

    // Progressbar animation
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }
}
